const btn = document.querySelectorAll("button");

btn.forEach(function (button, index) {
  button.addEventListener("click", function (event) {
    {
      var btnItem = event.target;
      var product = btnItem.parentElement;
      var productImg = product.querySelector("img").src;
      var productName = product.querySelector("h1").innerText;
      var productPrice = product.querySelector("span").innerText;

      // console.log(productImg, productName, productPrice);
      addcart(productImg, productName, productPrice);
    }
  });
});

function addcart(productImg, productName, productPrice) {
  var addtr = document.createElement("tr");
  var cartItem = document.querySelector("tbody tr");
  for (var i = 0; i < cartItem; i++) {
    var productT = document.querySelectorAll(".title");
    if (productT[i].innerHTML == productName) {
      alert("Sản phẩm của bạn đã có trong giỏ hàng");
      return;
    }
  }

  var trContent =
    '<tr><td style="display: flex; align-items: center;"> <img style="width: 70px" src="' +
    productImg +
    '"alt=""> <span class = "title"> <span class = "title">' +
    productName +
    " </span>";
  "</td><td><p><span class = ' prices" +
    productPrice +
    ' </span></p><sup> đ </sup></td> <td> <inputstyle="width: 30px; outline: none" type="number" value="1" min="1" /></td><td style="cursor: pointer;"> <span class = "cart-delete;"> <i class="fa fa-trash-alt"></i> </span></td> </tr>';

  addtr.innerHTML = trContent;
  var cartTable = document.querySelector("tbody");

  cartTable.append(addtr);

  cartTotal();
}

//totalCart

function cartTotal() {
  var cartItem = document.querySelectorAll("tbody tr");

  var totalC = 0;

  for (var i = 0; i < cartItem.length; i++) {
    var inputValue = cartItem[i].querySelector("input").value;
    var productPrice = cartItem[i].querySelector("prices").innerHTML;

    totalA = inputValue * productPrice * 1000;

    totalC = totalC + totalA;
  }

  var cartTotalA = document.querySelector(".price-total span");
  var cartPrice = document.querySelector(".cart-icon span");

  cartTotalA.innerHTML = totalC.toLocaleString("de-DE");
  cartPrice.innerHTML = totalC.toLocaleString("de-DE");

  deleteCart();
  inputChange();
}
function deleteCart() {
  var cartItem = document.querySelector("tbody tr");
  for (i = 0; i < cartItem.length; i++) {
    var productT = document.querySelectorAll(".cart-delete");
    productT[i].addEventListener("click", function (event) {
      var cartDelete = event.target;
      var cartItemR = cartDelete.parentElement;
      cartItemR.remove();
      cartTotal();
    });
  }
}

function inputChange() {
  var cartItem = document.querySelectorAll("tbody tr");
  for (var i = 0; i < cartItem.length; i++) {
    var inputValue = cartItem[i].querySelector("input");
    inputValue.addEventListener("change", function (event) {
      // cartItemR.remove();
      cartTotal();
    });
  }
}

const cartbtn = document.querySelector(".fa-times");
const cartshow = document.querySelector(".fa-shopping-cart");
cartshow.addEventListener("click", function () {
  console.log(cartshow);
  document.querySelector(".cart").style.right = "0";
});

cartbtn.addEventListener("click", function () {
  console.log(cartshow);
  document.querySelector(".cart").style.right = "-100%";
});
